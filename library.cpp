#include "library.h"

#include <windows.h>
#include <d3d9.h>
#include <d3dx9.h>

#include <string>
#include <filesystem>

using namespace std::string_literals;
namespace fs = std::filesystem;

namespace {
	constexpr auto kNetGameOffset = 0x26E8DC;
	constexpr auto kTextDrawTexturesOffset = 0x26B2B8;
	constexpr auto kLibraryName = "TextdrawStore";

	std::uintptr_t getSampLibrary() {
		static std::uintptr_t cache_addr = 0;
		if ( !cache_addr ) {
			auto lib = GetModuleHandleA( "samp" );
			if ( lib && lib != INVALID_HANDLE_VALUE )
				cache_addr = reinterpret_cast<std::uintptr_t>(lib);
		}
		return cache_addr;
	}

	NetGame *getNetGame() {
		auto samp = getSampLibrary();
		if ( !samp )
			return nullptr;
		return *reinterpret_cast<NetGame **>(samp + kNetGameOffset);
	}

	TextdrawPool *getTextDrawPool() {
		auto pNetGame = getNetGame();
		if ( !pNetGame || !pNetGame->pPools )
			return nullptr;

		return pNetGame->pPools->pTextdraw;
	}

	CSprite2d **getTextDrawTextures() {
		auto samp = getSampLibrary();
		if ( !samp )
			return nullptr;
		return reinterpret_cast<CSprite2d **>(samp + kTextDrawTexturesOffset);
	}
}

TextDraw *getTextDraw( int id ) {
	auto pool = getTextDrawPool();
	if ( !pool || !pool->iIsListed[id] )
		return nullptr;
	return pool->iPlayerTextDraw[id];
}

int getTextDrawTextureIndex( TextDraw *textDraw ) {
	if ( !textDraw )
		return -1;
	return textDraw->index;
}

IDirect3DBaseTexture9 *getTextDrawTextureByTextureIndex( int textureIndex ) {
	if ( textureIndex == -1 )
		return nullptr;
	auto textures = getTextDrawTextures();
	if ( !textures )
		return nullptr;
	auto sprite = textures[textureIndex];
	if ( !sprite || !sprite->texture || !sprite->texture->raster )
		return nullptr;
	return sprite->texture->raster->d3d9Texture;
}

IDirect3DBaseTexture9 *getTextDrawTextureById( int id ) {
	if ( id < 0 || id > 2304 )
		return nullptr;
	auto textDraw = getTextDraw( id );
	auto textureIndex = getTextDrawTextureIndex( textDraw );
	return getTextDrawTextureByTextureIndex( textureIndex );
}

bool storeTextDraw( int id, const char *path ) {
	auto texture = getTextDrawTextureById( id );
	if ( !texture )
		return false;
	auto hr = D3DXSaveTextureToFileA( path, D3DXIFF_PNG, texture, nullptr );
	return hr == D3D_OK;
}

bool storeTextDraw2( int id ) {
	if ( !fs::exists( kLibraryName ) || !fs::is_directory( kLibraryName ) ) {
		if ( !fs::is_directory( kLibraryName ) )
			fs::remove_all( kLibraryName );
		fs::create_directory( kLibraryName );
	}
	return storeTextDraw( id, ( kLibraryName + "/"s + std::to_string( id ) + ".png" ).c_str() );
}
