cmake_minimum_required(VERSION 3.21)
project(TexdrawStore)

set(CMAKE_CXX_STANDARD 20)

add_library(TexdrawStore SHARED library.cpp)
target_link_libraries(TexdrawStore PRIVATE d3dx9)
