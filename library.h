#ifndef TEXDRAWSTORE_LIBRARY_H
#define TEXDRAWSTORE_LIBRARY_H

#include <cstddef>

struct RwRaster {
	int parent;
	int cpPixels;
	int palette;
	int width;
	int height;
	int depth;
	int stride;
	short nOffsetX;
	short nOffsetY;
	char cType;
	char cFlags;
	char privateFlags;
	char cFormat;
	int originalPixels;
	int originalWidth;
	int originalHeight;
	int oriiginalStride;
#ifdef _MSC_VER

	struct IDirect3DBaseTexture9 *d3d9Texture;

#else

	class IDirect3DBaseTexture9 *d3d9Texture;

#endif
};
static_assert( sizeof( RwRaster ) == 0x38 );
static_assert( offsetof( RwRaster, d3d9Texture ) == 0x34 );

struct RwTexture {
	struct RwRaster *raster;
	int dict;
	char gap8[8];
	char name[32];
	char mask[32];
	char filterAddressing;
	int refCount;
};
static_assert( sizeof( RwTexture ) == 0x58 );
static_assert( offsetof( RwTexture, raster ) == 0x00 );

struct CSprite2d {
	RwTexture *texture;
};
static_assert( sizeof( CSprite2d ) == 0x4 );
static_assert( offsetof( CSprite2d, texture ) == 0x00 );

#pragma pack(push, 1)
struct TextDraw {
	char szTextureName[801];
	char szString[1602];
	float fLetterWidth;
	float fLetterHeight;
	unsigned int dwLetterColor;
	char field_96F;
	char byteCenter;
	char byteBox;
	float fBoxSizeX;
	float fBoxSizeY;
	unsigned int dwBoxColor;
	char byteProportional;
	unsigned int dwShadowColor;
	char byteShadowSize;
	char byteOutline;
	char byteLeft;
	char byteRight;
	int iStyle;
	float fX;
	float fY;
	char field_993[8];
	int field_99B;
	int field_99F;
	int index;
	char bIsClickable;
	unsigned short sModel;
	float fRot[3];
	float fZoom;
	unsigned short sColor[2];
	char field_9BE;
	char bIsRendered;
	char bUseControlKeysInString;
	long rect[4];
	char bUseLetterColor2;
	unsigned int dwLetterColor2;
};
#pragma pack(pop)
static_assert( sizeof( TextDraw ) == 0x9D6 );
static_assert( offsetof( TextDraw, index ) == 0x9A3 );

struct TextdrawPool {
	int iIsListed[2304];
	TextDraw *iPlayerTextDraw[2304];
	TextDraw *textdraw[2304];
	char unk[5120];
};
static_assert( sizeof( TextdrawPool ) == 0x8000 );
static_assert( offsetof( TextdrawPool, iIsListed ) == 0x00 );
static_assert( offsetof( TextdrawPool, iPlayerTextDraw ) == 0x2400 );

struct Pools {
	struct Menu *pMenu;
	struct ActorPool *pActor;
	struct PlayerPool *pPlayer;
	struct VehiclePool *pVehicle;
	struct PickupPool *pPickup;
	struct ObjectPool *pObject;
	struct GangzonePool *pGangzone;
	struct TextLabelPool *pText3D;
	TextdrawPool *pTextdraw;
};
static_assert( sizeof( Pools ) == 0x24 );
static_assert( offsetof( Pools, pTextdraw ) == 0x20 );

#pragma pack(push, 1)
struct NetGame {
	char _pad0[20];
	void *pUnk0;
	struct stServerInfo *pServerInfo;
	char _pad1[16];
	struct RakClient *rakClient;
	char szIP[257];
	char szHostname[257];
	bool hookCarCollisions;
	bool m_bUpdateCameraTarget;
	bool m_bNoNameTagStatus;
	unsigned int ulPort;
	int m_bLanMode;
	unsigned int ulMapIcons[100];
	int iGameState;
	unsigned int m_dwLastConnectAttempt;
	struct stServerPresets *pSettings;
	bool needUpdateSomeTimer;
	unsigned int someTimer;
	Pools *pPools;
};
#pragma pack(pop)
static_assert( sizeof( NetGame ) == 0x3E2 );
static_assert( offsetof( NetGame, pPools ) == 0x3DE );

extern "C" __declspec( dllexport ) TextDraw *getTextDraw( int id );
extern "C" __declspec( dllexport ) int getTextDrawTextureIndex( TextDraw *textDraw );
extern "C" __declspec( dllexport ) IDirect3DBaseTexture9 *getTextDrawTextureByTextureIndex( int textureIndex );
extern "C" __declspec( dllexport ) IDirect3DBaseTexture9 *getTextDrawTextureById( int id );
extern "C" __declspec( dllexport ) bool storeTextDraw( int id, const char *path );
extern "C" __declspec( dllexport ) bool storeTextDraw2( int id );

#endif //TEXDRAWSTORE_LIBRARY_H
